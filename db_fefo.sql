-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2019 at 02:09 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_fefo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE IF NOT EXISTS `tb_barang` (
  `KodeBarang` varchar(15) NOT NULL,
  `Barang` varchar(100) NOT NULL,
  `IdKat` varchar(15) NOT NULL,
  `Satuan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`KodeBarang`, `Barang`, `IdKat`, `Satuan`) VALUES
('BRG201908001', 'FANTA GRAPE 390ML', 'CAT20190801', 'Pcs'),
('BRG201908002', 'FRISIAN FLAG COKLAT BANDED 6', 'CAT20190801', 'Pcs'),
('BRG201908003', 'FAYROUZ PINEAPPLE', 'CAT20190801', 'Pcs'),
('BRG201908004', 'BIG COLA', 'CAT20190801', 'Pcs'),
('BRG201908005', 'BIG COLA STRAWBERRY', 'CAT20190801', 'Pcs'),
('BRG201908006', 'GOOD TO GO BANANA STRAWBERRY', 'CAT20190801', 'Pcs'),
('BRG201908007', 'YOU C 1000 ORANGE', 'CAT20190801', 'Pcs'),
('BRG201908008', 'CAAYA TEH MELATI', 'CAT20190801', 'Pcs'),
('BRG201908009', 'CAAYA TOASTED RICE', 'CAT20190801', 'Pcs'),
('BRG201908010', 'FANTA GRAPE 1,5 LT', 'CAT20190801', 'Pcs'),
('BRG201908011', 'VEGIE FRUIT', 'CAT20190801', 'Pcs'),
('BRG201908012', 'HILO CHOCO AVOCADO', 'CAT20190801', 'Pcs'),
('BRG201908013', 'ZESTO MANGO', 'CAT20190801', 'Pcs'),
('BRG201908014', 'AQUA 600ML', 'CAT20190801', 'Pcs'),
('BRG201908015', 'GOOD TO GO CHOCO AVOCADO', 'CAT20190801', 'Pcs'),
('BRG201908016', 'YUZU ISOTONIC', 'CAT20190801', 'Pcs'),
('BRG201908017', 'CAAYA VANILA', 'CAT20190801', 'Pcs'),
('BRG201908018', 'BINTANG ZERO CAN', 'CAT20190801', 'Pcs'),
('BRG201908019', 'BINTANG RADLER CAN', 'CAT20190801', 'Pcs'),
('BRG201908020', 'THE JAVANA MELATI', 'CAT20190801', 'Pcs'),
('BRG201908021', 'CHILGO PISANG', 'CAT20190801', 'Pcs'),
('BRG201908022', 'CHILGO MELON', 'CAT20190801', 'Pcs'),
('BRG201908023', 'NESTLE ACTICOR AVOCADO', 'CAT20190801', 'Pcs'),
('BRG201908024', 'NESTLE ACTICOR GREEN TEA LATTE', 'CAT20190801', 'Pcs'),
('BRG201908025', 'NESTLE ACTICOR  BANANA', 'CAT20190801', 'Pcs'),
('BRG201908026', 'NESTLE ACTICOR  CHOCO', 'CAT20190801', 'Pcs'),
('BRG201908027', 'VIT LEVITE BERRIES', 'CAT20190801', 'Pcs'),
('BRG201908028', 'VIT LEVITE LYCHEE', 'CAT20190801', 'Pcs'),
('BRG201908029', 'FROZEN AIR MINERAL 1,5 LT', 'CAT20190801', 'Pcs'),
('BRG201908030', 'LE MINERALE 600ML', 'CAT20190801', 'Pcs'),
('BRG201908031', 'NESCAFE LIVELY YUZU', 'CAT20190801', 'Pcs'),
('BRG201908034', 'CHOCOLATOS DRINK RTD', 'CAT20190801', 'Pcs'),
('BRG201908035', 'THE PUCUK 350ML', 'CAT20190801', 'Pcs'),
('BRG201908036', 'MIZONE ACTIV LYCHEE LEMON 500ML', 'CAT20190801', 'Pcs'),
('BRG201908037', 'MIZONE MOOD UP CRANBERRY 500M', 'CAT20190801', 'Pcs'),
('BRG201908038', 'MIZONE BREAK FREE CHERRY BLOSSOM 500ML ', 'CAT20190801', 'Pcs'),
('BRG201908039', 'MIZONE MOVE ON STARFRUIT 500ML ', 'CAT20190801', 'Pcs'),
('BRG201908040', 'ZESTO ORANGE', 'CAT20190801', 'Pcs'),
('BRG201908041', 'ZESTO GUAVA', 'CAT20190801', 'Pcs'),
('BRG201908042', 'GARUDA ROSTA WAGYU', 'CAT20190802', 'Pcs');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE IF NOT EXISTS `tb_kategori` (
  `IdKat` varchar(15) NOT NULL,
  `Kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`IdKat`, `Kategori`) VALUES
('CAT20190801', 'FOOD - Beverages'),
('CAT20190802', 'FOOD - Snack'),
('CAT20190803', 'FOOD - Syrup');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemasukan`
--

CREATE TABLE IF NOT EXISTS `tb_pemasukan` (
  `Noref` varchar(15) NOT NULL,
  `EntryDate` date NOT NULL,
  `KodeBarang` varchar(15) NOT NULL,
  `Qty` int(10) NOT NULL,
  `ExpDate` date NOT NULL,
  `IdSupplier` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pemasukan`
--

INSERT INTO `tb_pemasukan` (`Noref`, `EntryDate`, `KodeBarang`, `Qty`, `ExpDate`, `IdSupplier`) VALUES
('IN201908001', '2019-08-14', 'BRG201908001', 9, '2019-07-19', 'SUP201908001'),
('IN201908002', '2019-08-14', 'BRG201908002', 1, '2019-07-19', 'SUP201908001'),
('IN201908003', '2019-08-14', 'BRG201908003', 6, '2019-07-19', 'SUP201908001'),
('IN201908004', '2019-08-14', 'BRG201908004', 10, '2019-07-19', 'SUP201908001'),
('IN201908005', '2019-08-14', 'BRG201908005', 10, '2019-07-19', 'SUP201908001'),
('IN201908006', '2019-08-14', 'BRG201908006', 57, '2019-07-19', 'SUP201908001'),
('IN201908007', '2019-08-14', 'BRG201908007', 30, '2019-07-19', 'SUP201908001'),
('IN201908008', '2019-08-14', 'BRG201908008', 120, '2019-08-19', 'SUP201908001'),
('IN201908009', '2019-08-14', 'BRG201908009', 120, '2019-08-19', 'SUP201908001'),
('IN201908010', '2019-08-14', 'BRG201908010', 11, '2019-08-19', 'SUP201908001'),
('IN201908011', '2019-08-14', 'BRG201908011', 5, '2019-08-19', 'SUP201908001'),
('IN201908012', '2019-08-14', 'BRG201908012', 18, '2019-08-19', 'SUP201908001'),
('IN201908013', '2019-08-14', 'BRG201908013', 4, '2019-08-19', 'SUP201908001'),
('IN201908014', '2019-08-14', 'BRG201908014', 216, '2019-08-19', 'SUP201908001'),
('IN201908015', '2019-08-14', 'BRG201908015', 13, '2019-08-19', 'SUP201908001'),
('IN201908016', '2019-08-14', 'BRG201908031', 17, '2019-09-19', 'SUP201908001'),
('IN201908017', '2019-08-14', 'BRG201908042', 18, '2019-10-19', 'SUP201908001'),
('IN201908018', '2019-08-14', 'BRG201908014', 11, '2019-10-19', 'SUP201908001'),
('IN201908019', '2019-08-14', 'BRG201908034', 48, '2019-11-19', 'SUP201908001'),
('IN201908020', '2019-08-14', 'BRG201908035', 48, '2019-11-19', 'SUP201908001'),
('IN201908021', '2019-08-14', 'BRG201908036', 12, '2019-11-19', 'SUP201908001'),
('IN201908022', '2019-08-14', 'BRG201908037', 12, '2019-11-19', 'SUP201908001'),
('IN201908023', '2019-08-14', 'BRG201908038', 12, '2019-11-19', 'SUP201908001'),
('IN201908024', '2019-08-14', 'BRG201908039', 12, '2019-11-19', 'SUP201908001'),
('IN201908025', '2019-08-14', 'BRG201908040', 4, '2019-11-19', 'SUP201908001'),
('IN201908026', '2019-08-14', 'BRG201908041', 4, '2019-11-19', 'SUP201908001'),
('IN201908027', '2019-08-14', 'BRG201908016', 24, '2019-09-19', 'SUP201908001'),
('IN201908028', '2019-08-14', 'BRG201908017', 120, '2019-09-19', 'SUP201908001'),
('IN201908029', '2019-08-14', 'BRG201908018', 1, '2019-09-19', 'SUP201908001'),
('IN201908030', '2019-08-14', 'BRG201908019', 1, '2019-09-19', 'SUP201908001'),
('IN201908031', '2019-08-14', 'BRG201908020', 17, '2019-09-19', 'SUP201908001'),
('IN201908032', '2019-08-14', 'BRG201908021', 2, '2019-09-19', 'SUP201908001'),
('IN201908033', '2019-08-14', 'BRG201908022', 2, '2019-09-19', 'SUP201908001'),
('IN201908034', '2019-08-14', 'BRG201908023', 5, '2019-09-19', 'SUP201908001'),
('IN201908035', '2019-08-14', 'BRG201908024', 6, '2019-09-19', 'SUP201908001'),
('IN201908036', '2019-08-14', 'BRG201908025', 5, '2019-09-19', 'SUP201908001'),
('IN201908037', '2019-08-14', 'BRG201908026', 6, '2019-09-19', 'SUP201908001'),
('IN201908038', '2019-08-14', 'BRG201908027', 3, '2019-09-19', 'SUP201908001'),
('IN201908039', '2019-08-14', 'BRG201908028', 2, '2019-09-19', 'SUP201908001'),
('IN201908040', '2019-08-14', 'BRG201908029', 12, '2019-09-19', 'SUP201908001'),
('IN201908041', '2019-08-14', 'BRG201908030', 24, '2019-09-19', 'SUP201908001');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengeluaran`
--

CREATE TABLE IF NOT EXISTS `tb_pengeluaran` (
  `Noref` varchar(15) NOT NULL,
  `OutDate` date NOT NULL,
  `Keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengeluaran`
--

INSERT INTO `tb_pengeluaran` (`Noref`, `OutDate`, `Keterangan`) VALUES
('OUT201908001', '2019-08-14', 'Pengeluaran 1'),
('OUT201908002', '2019-08-14', 'Pengeluaran Sebagian');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengeluaran_det`
--

CREATE TABLE IF NOT EXISTS `tb_pengeluaran_det` (
`Id` int(10) NOT NULL,
  `Noref` varchar(15) NOT NULL,
  `KodeBarang` varchar(15) NOT NULL,
  `Qty` int(10) NOT NULL,
  `NorefMasuk` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengeluaran_det`
--

INSERT INTO `tb_pengeluaran_det` (`Id`, `Noref`, `KodeBarang`, `Qty`, `NorefMasuk`) VALUES
(6, 'OUT201908001', 'BRG201908004', 10, 'IN201908004'),
(7, 'OUT201908002', 'BRG201908014', 100, 'IN201908014');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengguna`
--

CREATE TABLE IF NOT EXISTS `tb_pengguna` (
  `IdPengguna` varchar(15) NOT NULL,
  `NamaPengguna` varchar(100) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Role` enum('Admin','User') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengguna`
--

INSERT INTO `tb_pengguna` (`IdPengguna`, `NamaPengguna`, `Username`, `Password`, `Role`) VALUES
('USER201908001', 'Dea Apriliani', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin'),
('USER201908002', 'Asri', 'asri', '371e3678e35662cc9862fdb96fece88b', 'User'),
('USER201908003', 'Khabibah', 'khabibah', '955f20cf9b1a70a7a6d24529d0d62376', 'User'),
('USER201908004', 'Hany', 'hany', 'f88e6e106ca9911db8034f61df7f7f23', 'User'),
('USER201908005', 'Vinda', 'vinda', '472b0fa7e71bc6d4f438333b1f0decd3', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE IF NOT EXISTS `tb_supplier` (
  `IdSupplier` varchar(15) NOT NULL,
  `Supplier` varchar(100) NOT NULL,
  `Alamat` text NOT NULL,
  `Phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`IdSupplier`, `Supplier`, `Alamat`, `Phone`) VALUES
('SUP201908001', 'Supplier 1', 'Bandung', '085721322227');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
 ADD PRIMARY KEY (`KodeBarang`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
 ADD PRIMARY KEY (`IdKat`);

--
-- Indexes for table `tb_pemasukan`
--
ALTER TABLE `tb_pemasukan`
 ADD PRIMARY KEY (`Noref`);

--
-- Indexes for table `tb_pengeluaran`
--
ALTER TABLE `tb_pengeluaran`
 ADD PRIMARY KEY (`Noref`);

--
-- Indexes for table `tb_pengeluaran_det`
--
ALTER TABLE `tb_pengeluaran_det`
 ADD PRIMARY KEY (`Id`), ADD KEY `Noref` (`Noref`);

--
-- Indexes for table `tb_pengguna`
--
ALTER TABLE `tb_pengguna`
 ADD PRIMARY KEY (`IdPengguna`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
 ADD PRIMARY KEY (`IdSupplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_pengeluaran_det`
--
ALTER TABLE `tb_pengeluaran_det`
MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_pengeluaran_det`
--
ALTER TABLE `tb_pengeluaran_det`
ADD CONSTRAINT `tb_pengeluaran_det_ibfk_1` FOREIGN KEY (`Noref`) REFERENCES `tb_pengeluaran` (`Noref`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
