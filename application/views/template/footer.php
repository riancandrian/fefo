<!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© 2019 <a href="#">Remark</a></div>
      <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="#">Creation Studio</a>
      </div>
    </footer>
    <!-- Core  -->
    <script src="<?=base_url('assets/');?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/jquery/jquery.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/animsition/animsition.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/waves/waves.js"></script>

    <!-- Plugins -->
    <script src="<?=base_url('assets/');?>global/vendor/switchery/switchery.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/intro-js/intro.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/screenfull/screenfull.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/select2/select2.full.min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>

    <script src="<?=base_url('assets/');?>global/vendor/chartist/chartist.min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/matchheight/jquery.matchHeight-min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/peity/jquery.peity.min.js"></script>

    <script src="<?=base_url('assets/');?>global/vendor/datatables.net/jquery.dataTables.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-buttons/buttons.html5.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-buttons/buttons.flash.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-buttons/buttons.print.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/asrange/jquery-asRange.min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/bootbox/bootbox.js"></script>

    <!-- Scripts -->
    <script src="<?=base_url('assets/');?>global/js/Component.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Base.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Config.js"></script>

    <script src="<?=base_url();?>assets/js/Section/Menubar.js"></script>
    <script src="<?=base_url();?>assets/js/Section/Sidebar.js"></script>
    <script src="<?=base_url();?>assets/js/Section/PageAside.js"></script>
    <script src="<?=base_url();?>assets/js/Plugin/menu.js"></script>

    <!-- Config -->
    <script src="<?=base_url('assets/');?>global/js/config/colors.js"></script>
    <script src="<?=base_url();?>assets/js/config/tour.js"></script>
    <script>Config.set('assets/', '<?=base_url();?>assets');</script>

    <!-- Page -->
    <script src="<?=base_url();?>assets/js/Site.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/asscrollable.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/slidepanel.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/switchery.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/jquery-placeholder.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/material.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/select2.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/bootstrap-sweetalert.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="<?=base_url('assets/');?>examples/js/advanced/bootbox-sweetalert.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/matchheight.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/jvectormap.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/peity.js"></script>
    <script src="<?=base_url('assets/');?>examples/js/dashboard/v1.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/datatables.js"></script>
    <script src="<?=base_url('assets/');?>examples/js/tables/datatable.js"></script>
    <script src="<?=base_url('assets/');?>examples/js/uikit/icon.js"></script>

    <script>
      (function(document, window, $){
        'use strict';

        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
  </body>
</html>
