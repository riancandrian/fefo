
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Free Goods MD</title>

    <link rel="apple-touch-icon" href="<?=base_url();?>assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?=base_url();?>assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/site.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/skins/orange.css">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/waves/waves.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/bootstrap-sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/chartist/chartist.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/jvectormap/jquery-jvectormap.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/examples/css/dashboard/v1.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/examples/css/tables/datatable.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="<?=base_url();?>assets/global/fonts/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <input type="hidden" value="<?=base_url(); ?>" id="base_url">

    <!-- Scripts -->
    <script src="<?=base_url();?>assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition">

    <nav class="site-navbar navbar navbar-default navbar-inverse navbar-fixed-top navbar-mega" role="navigation">

      <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
          data-toggle="menubar">
          <span class="sr-only">Toggle navigation</span>
          <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
          data-toggle="collapse">
          <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
          <img class="navbar-brand-logo" src="<?=base_url();?>assets/images/logo.png" title="Remark">
          <span class="navbar-brand-text hidden-xs-down"> Free Goods MD</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
          data-toggle="collapse">
          <span class="sr-only">Toggle Search</span>
          <i class="icon md-search" aria-hidden="true"></i>
        </button>
      </div>

      <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">

          <!-- LOAD FOR NOTIF -->
          <?php

            $notif = 0;

            //cek barang sudah EXPIRE
            $c2_expire = $this->db->query("SELECT * from tb_barang
                      JOIN tb_pemasukan ON tb_pemasukan.KodeBarang = tb_barang.KodeBarang
                      LEFT JOIN tb_pengeluaran_det ON tb_pengeluaran_det.NorefMasuk = tb_pemasukan.Noref
                      WHERE ExpDate < CURDATE()
                      AND (tb_pemasukan.Qty - ifnull(tb_pengeluaran_det.Qty, 0)) > 0
                      ")->num_rows();
            if($c2_expire > 0){$notif = $notif + 1;}else{$notif = $notif;}

            $w_expire = $this->db->query("SELECT * from tb_barang
                      JOIN tb_pemasukan ON tb_pemasukan.KodeBarang = tb_barang.KodeBarang
                      LEFT JOIN tb_pengeluaran_det ON tb_pengeluaran_det.NorefMasuk = tb_pemasukan.Noref
                      WHERE ExpDate BETWEEN CURDATE() AND date_add(curdate(), INTERVAL 1 MONTH)
                      AND (tb_pemasukan.Qty - ifnull(tb_pengeluaran_det.Qty, 0)) > 0")->num_rows();
            if($w_expire > 0){$notif = $notif + 1;}else{$notif = $notif;}
          ?>
          <!-- Navbar Toolbar Right -->
          <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Notifications"
                aria-expanded="false" data-animation="scale-up" role="button">
                <i class="icon md-notifications" aria-hidden="true"></i>
                <span class="badge badge-pill badge-info up"><?=$notif?></span>
              </a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                <div class="dropdown-menu-header">
                  <h5>NOTIFIKASI</h5>
                  <span class="badge badge-round badge-info">New <?=$notif?></span>
                </div>

                <div class="list-group">
                  <div data-role="container">
                    <div data-role="content">
                      <!-- CEK BARANG SUDAH EXPIRE -->
                      <?php if($c2_expire > 0):?>
                      <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                        <div class="media">
                          <div class="pr-10">
                            <i class="icon fa-warning bg-red-600 white icon-circle" aria-hidden="true"></i>
                          </div>
                          <div class="media-body">
                            <h6 class="media-heading"><?=$c2_expire?> barang telah expire, segera cek.</h6>
                            <time class="media-meta" datetime="2017-06-12T20:50:48+08:00">5 hours ago</time>
                          </div>
                        </div>
                      </a>
                      <?php endif;?>

                      <!-- CEK BARANG AKAN EXPIRE -->
                      <?php if($w_expire > 0):?>
                      <a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
                        <div class="media">
                          <div class="pr-10">
                            <i class="icon fa-warning bg-orange-600 white icon-circle" aria-hidden="true"></i>
                          </div>
                          <div class="media-body">
                            <h6 class="media-heading"><?=$w_expire?> barang akan segera Expire, segera cek.</h6>
                            <time class="media-meta" datetime="2017-06-11T18:29:20+08:00">2 days ago</time>
                          </div>
                        </div>
                      </a>
                      <?php endif;?>
                    </div>
                  </div>
                </div>
                <div class="dropdown-menu-footer">
                  <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                    <i class="icon md-settings" aria-hidden="true"></i>
                  </a>
                  <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                    All notifications
                  </a>
                </div>
              </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                data-animation="scale-up" role="button">
                <span class="avatar avatar-online">
                  <img src="<?=base_url();?>assets/global/portraits/xx.png" alt="...">
                  <i></i>
                </span>
              </a>
              <div class="dropdown-menu" role="menu">
                <a class="dropdown-item" href="<?=base_url('login/logout')?>" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
              </div>
            </li>
          </ul>
          <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->

        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
          <form role="search">
            <div class="form-group">
              <div class="input-search">
                <i class="input-search-icon md-search" aria-hidden="true"></i>
                <input type="text" class="form-control" name="site-search" placeholder="Search...">
                <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                  data-toggle="collapse" aria-label="Close"></button>
              </div>
            </div>
          </form>
        </div>
        <!-- End Site Navbar Seach -->
      </div>
    </nav>

    <div class="site-menubar">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-item">
                <a class="animsition-link" href="<?=base_url('dashboard')?>">
                    <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                    <span class="site-menu-title">Dashboard</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon fa-cogs" aria-hidden="true"></i>
                  <span class="site-menu-title">Master</span>
                </a>

                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?=base_url('kategori')?>">
                      <span class="site-menu-title">Kategori</span>
                    </a>
                  </li>

                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?=base_url('pengguna')?>">
                      <span class="site-menu-title">Pengguna</span>
                    </a>
                  </li>

                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?=base_url('supplier')?>">
                      <span class="site-menu-title">Supplier</span>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="site-menu-item">
                <a class="animsition-link" href="<?=base_url('barang')?>">
                  <i class="site-menu-icon fa-cubes" aria-hidden="true"></i>
                  <span class="site-menu-title">Barang</span>
                </a>
              </li>

              <li class="site-menu-item">
                <a class="animsition-link" href="<?=base_url('pemasukan')?>">
                  <i class="site-menu-icon fa-shopping-basket" aria-hidden="true"></i>
                  <span class="site-menu-title">Pemasukan</span>
                </a>
              </li>

              <li class="site-menu-item">
                <a class="animsition-link" href="<?=base_url('pengeluaran')?>">
                  <i class="site-menu-icon fa-truck" aria-hidden="true"></i>
                  <span class="site-menu-title">Pengeluaran</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon fa-book" aria-hidden="true"></i>
                  <span class="site-menu-title">Laporan</span>
                </a>

                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?=base_url('laporan/persediaan')?>">
                      <span class="site-menu-title">Persediaan Barang</span>
                    </a>
                  </li>

                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?=base_url('laporan/pemasukan')?>">
                      <span class="site-menu-title">Pemasukan Barang</span>
                    </a>
                  </li>

                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?=base_url('laporan/pengeluaran')?>">
                      <span class="site-menu-title">Pengeluaran Barang</span>
                    </a>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </div>
      </div>
    </div>
