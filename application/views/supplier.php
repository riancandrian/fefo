<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-header">
    <h1 class="page-title">Supplier</h1>
  </div>

  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <button type="button" class="btn btn-info" data-target="#modal-form" data-toggle="modal"><i class="icon fa-plus-circle"></i> Tambah Data</button>
        </div>
      </div>

      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
              <div class="example table-responsive">
                <table class="table table-hover" id="tb_data">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th width="10%">ID Supplier</th>
                      <th width="25%">Supplier</th>
                      <th width="20%">Alamat</th>
                      <th width="20%">Phone</th>
                      <th width="20%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-form" aria-hidden="false">
  <div class="modal-dialog">
    <form class="modal-content" autocomplete="off" id="f_data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Form Supplier</h4>
      </div>

      <div class="modal-body">
        <!-- ID Supplier -->
        <input type="hidden" class="form-control" name="IdSupplier"  readonly>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control" name="Supplier" required/>
          <label class="floating-label">Nama Supplier</label>
        </div>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control" name="Alamat" required/>
          <label class="floating-label">Alamat</label>
        </div>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="number" class="form-control" name="Phone" />
          <label class="floating-label">Phone</label>
        </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </form>
  </div>
</div>

<?php include 'template/footer.php'; ?>

<script type="text/javascript">
  var url = '<?=base_url(); ?>';
  load();

  $(document).ready(function(){
    $('form#f_data').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
          url: url + 'supplier/saveorup_supplier',
          type: 'POST',
          data: formData,
          success: function (data) {
              var jsonData = JSON.parse(data);

              if(jsonData.success){
                swal({
                  title: "Selamat!",
                  text: "Data berhasil disimpan!",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                  confirmButtonText: 'OK',
                  closeOnConfirm: false
                });

                $('form#f_data').trigger('reset');
                $('#modal-form').modal('hide');

                load();
              }else{
                alert("Data Gagal disimpan");
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
    });
  });

  function load(){
    $.ajax({
      url : url + 'supplier/get',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);
        var nomor    = 1;
        $('#tb_data tbody').empty();

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            var tr = "<tr>\
                        <td>"+(nomor)+"</td>\
                        <td>"+val.IdSupplier+"</td>\
                        <td>"+val.Supplier+"</td>\
                        <td>"+val.Alamat+"</td>\
                        <td>"+val.Phone+"</td>\
                        <td>\
                          <button type='button' class='btn btn-sm btn-flat btn-warning' onclick={forEdit('"+val.IdSupplier+"')}><i class='icon fa-edit'></i> Edit</button>\
                          <button type='button' class='btn btn-sm btn-flat btn-danger' onclick={hapus('"+val.IdSupplier+"')}><i class='icon fa-trash'></i> Hapus</button>\
                        </td>\
                      </tr>";

            $('#tb_data tbody').append(tr);
            nomor++;
          });
        }
      }
    })
  }

  function forEdit(id){
    $.ajax({
      url : url + 'supplier/getById',
      data: {id: id},
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            $('input[name="IdSupplier"]').val(val.IdSupplier);
            $('input[name="Supplier"]').val(val.Supplier).change();
            $('input[name="Alamat"]').val(val.Alamat).change();
            $('input[name="Phone"]').val(val.Phone).change();
          });

          $('#modal-form').modal('show');
        }
      }
    })
  }

  function hapus(id){
    swal({
        title: "Apakah anda yakin?",
        text: "Anda tidak bisa mengembalikan data yang sudah di hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: 'Ya, hapus saja!',
        closeOnConfirm: false
      }, function () {
        $.ajax({
          url : url + 'supplier/hapus',
          type: 'POST',
          data: {id: id},
          success: function(respons){
            swal("Terhapus!", "Data sudah terhapus!", "success");
            load();
          }
        })
      });
  }
</script>
