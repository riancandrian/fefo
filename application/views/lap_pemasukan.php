<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Laporan Pemasukan Barang
        </h3>

        <div class="panel-actions">
          <?php
            $mulai = '';
            $akhir = '';

            if(isset($start)){
              $mulai = $start;
            }

            if(isset($end)){
              $akhir = $end;
            }

          ?>

          <a href="<?=base_url('laporan/pemasukan_pdf?start=').$mulai.'&end='.$akhir;?>" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-print"></i> Cetak</a>

          <!-- <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-print"></i> Cetak</button> -->
        </div>
      </div>

      <div class="panel-body">
        <div class="example-wrap">
          <div class="example">
            <form action="<?=base_url('laporan/filter_pemasukan')?>" method="post">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-daterange" data-plugin="datepicker">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="icon md-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" name="start" value="<?=(isset($start)) ? $start : '' ?>" />
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">to</span>
                      <input type="text" class="form-control" name="end" value="<?=(isset($end)) ? $end : '' ?>"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-1">
                  <button type="submit" class="btn btn-primary" name="button"><i class="fa fa-search"></i> Cari</button>
                </div>
              </div>
            </form>

            <div class="table-responsive">
              <br>
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th width="5%">No. Ref</th>
                    <th width="10%">Tgl. Masuk</th>
                    <th width="30%">Nama Barang</th>
                    <th width="10%">Qty</th>
                    <th width="10%">Tgl. Expire</th>
                    <th width="20%">Supplier</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($pemasukan as $row): ?>
                    <tr>
                      <td><?=$row->Noref?></td>
                      <td><?=$row->EntryDate?></td>
                      <td><?=$row->Barang?></td>
                      <td><?=$row->Qty?></td>
                      <td><?=$row->ExpDate?></td>
                      <td><?=$row->Supplier?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include 'template/footer.php'; ?>
