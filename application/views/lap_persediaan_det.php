<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Laporan Persediaan Barang
        </h3>

        <div class="panel-actions">
        </div>
      </div>

      <div class="panel-body">
        <div class="example-wrap">
          <div class="example">

            <div class="table-responsive">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th width="10%">Kode Barang</th>
                    <th width="30%">Nama Barang</th>
                    <th width="20%">Kategori</th>
                    <th width="10%">Qty</th>
                    <th width="20%">Satuan</th>
                    <th width="10%">Expire Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($detail as $row): ?>
                    <tr>
                      <td><?=$row->KodeBarang?></td>
                      <td><?=$row->Barang?></td>
                      <td><?=$row->Kategori?></td>
                      <td><?=$row->Sisa?></td>
                      <td><?=$row->Satuan?></td>
                      <td><?=$row->ExpDate?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include 'template/footer.php'; ?>
