<?php
    $pdf = new Pdf('L', 'A4');
    $pdf->SetTitle('Laporan Pemasukan');
    $pdf->SetTopMargin(15);
    $pdf->setFooterMargin(15);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetAutoPageBreak(TRUE, 15);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
    $pdf->AddPage();

    $pdf->SetFont('times', 12);

    $i=0;
    $periode = '';
    if($start){
      $periode = 'Periode '.$start.' s/d '.$end;
    }

    $html='<h3 align="center">LAPORAN PEMASUKAN BARANG</h3>
           <h3 align="center">'.$periode.'</h3>
            <table cellspacing="1" bgcolor="#666666" cellpadding="2">
                <tr bgcolor="#ffffff" style="font-weight: bold">
                  <th width="12%" align="center">Tgl. Masuk</th>
                  <th width="20%" align="center">Kode Barang</th>
                  <th width="30%" align="center">Nama Barang</th>
                  <th width="10%" align="center">Qty</th>
                  <th width="16%" align="center">Supplier</th>
                  <th width="12%" align="center">Exp Date</th>
                </tr>';
    $pdf->SetFont('times', 8);
      foreach ($datanya as $row)
      {
          $i++;

          $html.='<tr bgcolor="#ffffff">
                    <td>'.$row->EntryDate.'</td>
                    <td>'.$row->KodeBarang.'</td>
                    <td>'.$row->Barang.'</td>
                    <td>'.$row->Qty.'</td>
                    <td>'.$row->Supplier.'</td>
                    <td>'.$row->ExpDate.'</td>
                  </tr>';
      }
    $html.='</table>';
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->Output('Laporan Pemasukan.pdf', 'I');
?>
