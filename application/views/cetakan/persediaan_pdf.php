<?php
    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->SetTitle('Laporan Persediaan');
    $pdf->SetTopMargin(15);
    $pdf->setFooterMargin(15);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetAutoPageBreak(TRUE, 15);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
    $pdf->AddPage();

    $pdf->SetFont('times', 12);

    $i=0;
    $keterangan = '';
    $html='<h3 align="center">LAPORAN PERSEDIAAN BARANG</h3>
            <table cellspacing="1" bgcolor="#666666" cellpadding="2">
                <tr bgcolor="#ffffff" style="font-weight: bold">
                    <th width="18%" align="center">Kode Barang</th>
                    <th width="42%" align="center">Nama Barang</th>
                    <th width="20%" align="center">Kategori</th>
                    <th width="10%" align="center">Qty</th>
                    <th width="10%" align="center">Satuan</th>
                </tr>';
    $pdf->SetFont('times', 8);
      foreach ($datanya as $row)
      {
          $i++;

          $html.='<tr bgcolor="#ffffff">
                      <td>'.$row->KodeBarang.'</td>
                      <td>'.$row->Barang.'</td>
                      <td>'.$row->Kategori.'</td>
                      <td align="right">'.number_format($row->Sisa).'</td>
                      <td align="center">'.$row->Satuan.'</td>
                  </tr>';
      }
    $html.='</table>';
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->Output('Laporan Persediaan.pdf', 'I');
?>
