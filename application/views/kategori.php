<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-header">
    <h1 class="page-title">Kategori Barang</h1>
  </div>

  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <button type="button" class="btn btn-info" data-target="#modal-form" data-toggle="modal"><i class="icon fa-plus-circle"></i> Tambah Data</button>
        </div>
      </div>

      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
              <div class="example table-responsive">
                <table class="table table-striped" id="tb_data">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th width="10%">ID Kategori</th>
                      <th width="60%">Kategori</th>
                      <th width="25%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-form" aria-hidden="false">
  <div class="modal-dialog">
    <form class="modal-content" autocomplete="off" id="f_data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Form Kategori Barang</h4>
      </div>

      <div class="modal-body">
        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control" name="IdKat" value="Auto Generate" readonly>
          <label class="floating-label">ID Kategori</label>
        </div>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control" name="Kategori" value="" required/>
          <label class="floating-label">Nama Kategori</label>
        </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </form>
  </div>
</div>

<?php include 'template/footer.php'; ?>

<script type="text/javascript">
  var url = '<?=base_url(); ?>';
  load();

  $(document).ready(function(){
    $('form#f_data').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
          url: url + 'kategori/saveorup_kategori',
          type: 'POST',
          data: formData,
          success: function (data) {
              var jsonData = JSON.parse(data);

              if(jsonData.success){
                swal({
                  title: "Selamat!",
                  text: "Data berhasil disimpan!",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                  confirmButtonText: 'OK',
                  closeOnConfirm: false
                });

                $('form#f_data').trigger('reset');
                $('#modal-form').modal('hide');

                load();
              }else{
                alert("Data Gagal disimpan");
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
    });
  });

  function load(){
    $.ajax({
      url : url + 'kategori/get',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);
        var nomor    = 1;
        $('#tb_data tbody').empty();

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            var tr = "<tr>\
                        <td>"+(nomor)+"</td>\
                        <td>"+val.IdKat+"</td>\
                        <td>"+val.Kategori+"</td>\
                        <td>\
                          <button type='button' class='btn btn-sm btn-flat btn-warning' onclick={forEdit('"+val.IdKat+"')}><i class='icon fa-edit'></i> Edit</button>\
                          <button type='button' class='btn btn-sm btn-flat btn-danger' onclick={hapus('"+val.IdKat+"')}><i class='icon fa-trash'></i> Hapus</button>\
                        </td>\
                      </tr>";

            $('#tb_data tbody').append(tr);
            nomor++;
          });
        }
      }
    })
  }

  function forEdit(id){
    $.ajax({
      url : url + 'kategori/getById',
      data: {id: id},
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            $('input[name="IdKat"]').val(val.IdKat);
            $('input[name="Kategori"]').val(val.Kategori).change();
          });

          $('#modal-form').modal('show');
        }
      }
    })
  }

  function hapus(id){
    swal({
        title: "Apakah anda yakin?",
        text: "Anda tidak bisa mengembalikan data yang sudah di hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: 'Ya, hapus saja!',
        closeOnConfirm: false
      }, function () {
        $.ajax({
          url : url + 'kategori/hapus',
          type: 'POST',
          data: {id: id},
          success: function(respons){
            swal("Terhapus!", "Data sudah terhapus!", "success");
            load();
          }
        })
      });
  }
</script>
