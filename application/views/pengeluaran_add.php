<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content">
    <div class="panel">
      <form autocomplete="off" id="f_data">
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <div class="col-md-6">
              <div class="example-wrap">
                <h4 class="example-title">Input Pengeluaran</h4>
                <div class="example">
                  <div class="form-row">

                    <!-- ID Pengeluaran -->
                    <input type="hidden" name="Noref">

                    <div class="form-group form-material col-md-6">
                      <label class="form-control-label">Tanggal</label>
                      <input type="text" class="form-control" value="<?=date('m/d/Y')?>" name="OutDate" data-plugin="datepicker" readonly>
                    </div>

                    <div class="form-group form-material col-md-6">
                      <label class="form-control-label">Tujuan / Keterangan</label>
                      <input type="text" name="Keterangan" class="form-control" required>
                    </div>

                  </div>

                  <div class="form-row">
                    <div class="form-group form-material col-md-12">
                      <label class="form-control-label">Barang</label>
                      <select class="form-control" name="KodeBarang" data-plugin="select2" data-placeholder="Pilih Barang" data-allow-clear="true">
                        <option value="">Pilih Barang</option>
                        <?php foreach ($barang as $row): ?>
                          <option value="<?=$row->KodeBarang?>"><?=$row->Barang?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="example-wrap">
                <div class="table-responsive" style="max-height: 200px;">
                  <h4 class="example-title">List Stok Barang</h4>
                  <table class="table table-hover table-striped" id="listStok">
                    <thead>
                      <th>No.</th>
                      <th>Nama Barang</th>
                      <th>Qty</th>
                      <th>Tgl. Expire</th>
                      <th>Pick</th>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="row row-md">
            <div class="col-md-12">
              <div class="example-wrap">
                <div class="table-responsive" style="max-height: 200px;">
                  <h4 class="example-title">List Barang Keluar</h4>
                  <table class="table table-hover table-striped" id="tb_keluar">
                    <thead>
                      <th width="5%">No.</th>
                      <th width="65%">Nama Barang</th>
                      <th width="10%">Qty</th>
                      <th width="10%">Tgl. Expire</th>
                      <th width="10%"></th>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>

              <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Simpan Pengeluaran</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php include 'template/footer.php'; ?>

<script type="text/javascript">
  var url    = '<?=base_url(); ?>';
  var urutan = 1;

  $(document).ready(function(){

    $('select[name="KodeBarang"]').change(function(){
      var KodeBarang = $(this).val();

      $.ajax({
        url : url + 'pengeluaran/getListBarang',
        data: {kode : KodeBarang},
        type: 'POST',
        success: function(result){
          var jsonData = JSON.parse(result);
          var nomor    = 1;
          $('#listStok tbody').empty();

          if(jsonData.success){
            $.each(jsonData.data, function(key, val){

              var params = val.KodeBarang+'/'+val.Barang+'/'+val.Stok+'/'+val.ExpDate+'/'+val.Noref;

              var tr = "<tr>\
                          <td>"+nomor+"</td>\
                          <td>"+val.Barang+"</td>\
                          <td>"+val.Stok+"</td>\
                          <td>"+val.ExpDate+"</td>\
                          <td>\
                            <button type='button' class='btn btn-xs btn-flat btn-success' onclick='pick(\""+params+"\")'><i class='icon fa-check-square'></i></button>\
                          </td>\
                        </tr>";

              $('#listStok tbody').append(tr);
              nomor++;
            });
          }

        }

      })
    });

    $('form#f_data').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
          url: url + 'pengeluaran/saveorup_pengeluaran',
          type: 'POST',
          data: formData,
          success: function (data) {
              var jsonData = JSON.parse(data);

              if(jsonData.success){
                swal({
                  title: "Selamat!",
                  text: "Data berhasil disimpan!",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                  confirmButtonText: 'OK',
                  closeOnConfirm: false
                });

                $('form#f_data').trigger('reset');
                $('#modal-form').modal('hide');

                load();
              }else{
                alert("Data Gagal disimpan");
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
    });

  });

  function pick(params){
    var string = params.split("/");

    var row = "<tr>\
                  <td>"+urutan+"</td>\
                  <td>"+string[1]+"</td>\
                  <td>\
                      <input type='hidden' name='Kode[]' value='"+string[0]+"'>\
                      <input type='hidden' name='NorefMasuk[]' value='"+string[4]+"'>\
                      <input type='hidden' id='maxi-"+urutan+"' value='"+string[2]+"'>\
                      <input type='number' name='Qty[]' id='qty-"+urutan+"' class='form-control' value='"+string[2]+"' onChange='cekMaxi("+urutan+")'></td>\
                  <td>"+string[3]+"</td>\
                  <td></td>\
               </tr>";

    $('#tb_keluar tbody').append(row);
    urutan++;
  }

  function cekMaxi(urutan){
    var nilai = parseInt($('#qty-'+urutan+'').val());
    var maxi  = parseInt($('#maxi-'+urutan+'').val());

    if(nilai > maxi){
      $('#qty-'+urutan+'').val(maxi);
    }

  }
</script>
