<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Pemasukan Barang
        </h3>

        <div class="panel-actions">
          <button type="button" class="btn btn-sm btn-primary" data-target="#modal-form" data-toggle="modal"><i class="fa fa-plus-circle"></i> Tambah Pemasukan</button>
          <!-- <a class="panel-action icon md-fullscreen" data-toggle="panel-fullscreen" aria-hidden="true"></a>
          <a class="panel-action icon md-close" data-toggle="panel-close" aria-hidden="true"></a> -->
        </div>
      </div>

      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
              <div class="example table-responsive">
                <table class="table table-hover dataTable table-striped w-full" id="tb_data">
                  <thead>
                    <tr>
                      <th width="5%">No. Ref</th>
                      <th width="10%">Tgl. Masuk</th>
                      <th width="25%">Nama Barang</th>
                      <th width="10%">Qty</th>
                      <th width="10%">Tgl. Expire</th>
                      <th width="20%">Supplier</th>
                      <th width="20%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-form" aria-hidden="false">
  <div class="modal-dialog">
    <form class="modal-content" autocomplete="off" id="f_data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Form Pemasukan Barang</h4>
      </div>

      <div class="modal-body">
        <!-- ID Pemasukan -->
        <input type="hidden" name="Noref">

        <div class="form-group form-material">
          <label class="form-control-label">Tgl. Masuk</label>
          <input type="text" class="form-control" data-plugin="datepicker" name="EntryDate" value="<?=date('m/d/Y')?>" required>
        </div>

        <div class="form-group form-material">
          <label class="form-control-label">Nama Barang</label>
          <select class="form-control" data-plugin="select2" data-allow-clear="true" name="KodeBarang" required>
            <option value="">Pilih Barang</option>
            <?php foreach ($barang as $row): ?>
              <option value="<?=$row->KodeBarang?>"><?=$row->Barang?></option>
            <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group form-material">
          <label class="form-control-label">Supplier</label>
          <select class="form-control" data-plugin="select2" data-allow-clear="true" name="IdSupplier" required>
            <option value="">Pilih Supplier</option>
            <?php foreach ($supplier as $row): ?>
              <option value="<?=$row->IdSupplier?>"><?=$row->Supplier?></option>
            <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group form-material">
          <label class="form-control-label">Qty</label>
          <input type="number" class="form-control" placeholder="0" name="Qty" required>
        </div>

        <div class="form-group form-material">
          <label class="form-control-label">Tgl. Expire</label>
          <input type="text" class="form-control" data-plugin="datepicker" name="ExpDate" required>
        </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </form>
  </div>
</div>

<?php include 'template/footer.php'; ?>

<script type="text/javascript">
  var url = '<?=base_url(); ?>';
  load();

  $(document).ready(function(){
    $('form#f_data').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
          url: url + 'pemasukan/saveorup_pemasukan',
          type: 'POST',
          data: formData,
          success: function (data) {
              var jsonData = JSON.parse(data);

              if(jsonData.success){
                swal({
                  title: "Selamat!",
                  text: "Data berhasil disimpan!",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                  confirmButtonText: 'OK',
                  closeOnConfirm: false
                },function(){
                   location.reload();
                });

                // $('select[name="KodeBarang"]').val('').trigger('change');
                // $('select[name="IdSupplier"]').val('').trigger('change');
                //
                // $('form#f_data').trigger('reset');
                // $('#modal-form').modal('hide');
                //
                // load();
              }else{
                alert("Data Gagal disimpan");
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
    });
  });

  function load(){
    $.ajax({
      url : url + 'pemasukan/get',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);
        $('#tb_data tbody').empty();

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            var tr = "<tr>\
                        <td>"+val.Noref+"</td>\
                        <td>"+val.EntryDate+"</td>\
                        <td>"+val.Barang+"</td>\
                        <td>"+val.Qty+"</td>\
                        <td>"+val.ExpDate+"</td>\
                        <td>"+val.Supplier+"</td>\
                        <td>\
                          <button type='button' class='btn btn-sm btn-flat btn-warning' onclick={forEdit('"+val.Noref+"')}><i class='icon fa-edit'></i> Edit</button>\
                          <button type='button' class='btn btn-sm btn-flat btn-danger' onclick={hapus('"+val.Noref+"')}><i class='icon fa-trash'></i> Hapus</button>\
                        </td>\
                      </tr>";

            $('#tb_data tbody').append(tr);
          });

          $('#tb_data').dataTable();
        }
      }
    })
  }

  function forEdit(id){
    $.ajax({
      url : url + 'pemasukan/getById',
      data: {id: id},
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            $('input[name="Noref"]').val(val.Noref);
            $('select[name="KodeBarang"]').val(val.KodeBarang).change();
            $('select[name="IdSupplier"]').val(val.IdSupplier).change();
            $('input[name="Qty"]').val(val.Qty).change();
            $('input[name="ExpDate"]').val(val.ExpDate).change();
          });

          $('#modal-form').modal('show');
        }
      }
    })
  }

  function hapus(id){
    swal({
        title: "Apakah anda yakin?",
        text: "Anda tidak bisa mengembalikan data yang sudah di hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: 'Ya, hapus saja!',
        closeOnConfirm: false
      }, function () {
        $.ajax({
          url : url + 'pemasukan/hapus',
          type: 'POST',
          data: {id: id},
          success: function(respons){
            swal({
              title: "Terhapus!",
              text: "Data sudah terhapus!",
              type: "success",
              showCancelButton: false,
              confirmButtonClass: "btn-success",
              confirmButtonText: 'OK',
              closeOnConfirm: false
            },function(){
               location.reload();
            });
            load();
          }
        })
      });
  }
</script>
