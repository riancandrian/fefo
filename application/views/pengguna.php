<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-header">
    <h1 class="page-title">Pengguna</h1>
  </div>

  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <button type="button" class="btn btn-info" data-target="#modal-form" data-toggle="modal"><i class="icon fa-plus-circle"></i> Tambah Data</button>
        </div>
      </div>

      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
              <div class="example table-responsive">
                <table class="table table-striped" id="tb_data">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th width="10%">ID pengguna</th>
                      <th width="25%">Nama Lengkap</th>
                      <th width="20%">Username</th>
                      <th width="20%">Role</th>
                      <th width="20%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-form" aria-hidden="false">
  <div class="modal-dialog">
    <form class="modal-content" autocomplete="off" id="f_data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Form Pengguna</h4>
      </div>

      <div class="modal-body">
        <!-- ID PENGGUNA -->
        <input type="hidden" class="form-control" name="IdPengguna" readonly>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control" name="NamaPengguna" required/>
          <label class="floating-label">Nama Lengkap</label>
        </div>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control" name="Username" required/>
          <label class="floating-label">Username</label>
        </div>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="password" class="form-control" name="Password" required/>
          <label class="floating-label">Password</label>
        </div>

        <div class="form-group form-material floating" data-plugin="formMaterial">
          <select class="form-control" name="Role" required>
            <option value="">&nbsp;</option>
            <option value="Admin">Admin</option>
            <option value="User">User</option>
          </select>
          <label class="floating-label">Role Pengguna</label>
        </div>
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </form>
  </div>
</div>

<?php include 'template/footer.php'; ?>

<script type="text/javascript">
  var url = '<?=base_url(); ?>';
  load();

  $(document).ready(function(){
    $('form#f_data').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
          url: url + 'pengguna/saveorup_pengguna',
          type: 'POST',
          data: formData,
          success: function (data) {
              var jsonData = JSON.parse(data);

              if(jsonData.success){
                swal({
                  title: "Selamat!",
                  text: "Data berhasil disimpan!",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                  confirmButtonText: 'OK',
                  closeOnConfirm: false
                });

                $('form#f_data').trigger('reset');
                $('#modal-form').modal('hide');

                load();
              }else{
                alert("Data Gagal disimpan");
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
    });
  });

  function load(){
    $.ajax({
      url : url + 'pengguna/get',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);
        var nomor    = 1;
        $('#tb_data tbody').empty();

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            var tr = "<tr>\
                        <td>"+(nomor)+"</td>\
                        <td>"+val.IdPengguna+"</td>\
                        <td>"+val.NamaPengguna+"</td>\
                        <td>"+val.Username+"</td>\
                        <td>"+val.Role+"</td>\
                        <td>\
                          <button type='button' class='btn btn-sm btn-flat btn-warning' onclick={forEdit('"+val.IdPengguna+"')}><i class='icon fa-edit'></i> Edit</button>\
                          <button type='button' class='btn btn-sm btn-flat btn-danger' onclick={hapus('"+val.IdPengguna+"')}><i class='icon fa-trash'></i> Hapus</button>\
                        </td>\
                      </tr>";

            $('#tb_data tbody').append(tr);
            nomor++;
          });
        }
      }
    })
  }

  function forEdit(id){
    $.ajax({
      url : url + 'pengguna/getById',
      data: {id: id},
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            $('input[name="IdPengguna"]').val(val.IdPengguna);
            $('input[name="NamaPengguna"]').val(val.NamaPengguna).change();
            $('input[name="Username"]').val(val.Username).change();
            $('input[name="Password"]').attr('placeholder', 'Abaikan jika tidak ingin dirubah').change();
            $('select[name="Role"]').val(val.Role).change();
            $('input[name="Password"]').prop('required', false);
          });

          $('#modal-form').modal('show');
        }
      }
    })
  }

  function hapus(id){
    swal({
        title: "Apakah anda yakin?",
        text: "Anda tidak bisa mengembalikan data yang sudah di hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: 'Ya, hapus saja!',
        closeOnConfirm: false
      }, function () {
        $.ajax({
          url : url + 'pengguna/hapus',
          type: 'POST',
          data: {id: id},
          success: function(respons){
            swal("Terhapus!", "Data sudah terhapus!", "success");
            load();
          }
        })
      });
  }
</script>
