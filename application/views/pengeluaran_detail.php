<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Detail Pengeluaran Barang
        </h3>
      </div>

      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
              <div class="example table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th width="15%">Kode Barang</th>
                      <th width="30%">Nama Barang</th>
                      <th width="10%">Satuan</th>
                      <th width="10%">Qty</th>
                      <th width="20%">Tgl. Expire</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($detail as $row): ?>
                      <tr>
                        <td><?=$row->KodeBarang?></td>
                        <td><?=$row->Barang?></td>
                        <td><?=$row->Satuan?></td>
                        <td><?=$row->Qty?></td>
                        <td><?=$row->ExpDate?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include 'template/footer.php'; ?>
