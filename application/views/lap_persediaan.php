<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Laporan Persediaan Barang
        </h3>

        <div class="panel-actions">
          <a href="<?=base_url('laporan/persediaan_pdf')?>" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-print"></i> Cetak</a>
        </div>
      </div>

      <div class="panel-body">
        <div class="example-wrap">
          <div class="example">
            <!-- <div class="btn-group hidden-sm-down" id="exampleToolbar" role="group">
              <button type="button" class="btn btn-info btn-icon">
                <i class="icon md-plus" aria-hidden="true"></i>
              </button>
              <button type="button" class="btn btn-info btn-icon">
                <i class="icon md-favorite" aria-hidden="true"></i>
              </button>
              <button type="button" class="btn btn-info btn-icon">
                <i class="icon md-delete" aria-hidden="true"></i>
              </button>
            </div> -->

            <div class="table-responsive">
              <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                <thead>
                  <tr>
                    <th width="10%">Kode Barang</th>
                    <th width="30%">Nama Barang</th>
                    <th width="20%">Kategori</th>
                    <th width="10%">Qty</th>
                    <th width="20%">Satuan</th>
                    <th width="10%">Detail</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($barang as $row): ?>
                    <tr>
                      <td><?=$row->KodeBarang?></td>
                      <td><?=$row->Barang?></td>
                      <td><?=$row->Kategori?></td>
                      <td><?=$row->Sisa?></td>
                      <td><?=$row->Satuan?></td>
                      <td>
                        <a href="<?=base_url('laporan/persediaan_det/').$row->KodeBarang;?>" class="btn btn-sm btn-flat btn-success"><i class="icon fa-search"></i> Detail Stok</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include 'template/footer.php'; ?>
