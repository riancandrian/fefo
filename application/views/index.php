<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">

      <div class="col-xl-3 col-md-6">
        <div class="card card-shadow" id="widgetLineareaThree">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i> Pengguna
              </div>
              <span class="float-right grey-700 font-size-30"><?=$c_pengguna?></span>
            </div>
            <div class="ct-chart h-50"></div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-md-6">
        <div class="card card-shadow" id="widgetLineareaFour">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon fa-users grey-600 font-size-24 vertical-align-bottom mr-5"></i> Supplier
              </div>
              <span class="float-right grey-700 font-size-30"><?=$c_supplier?></span>
            </div>
            <div class="ct-chart h-50"></div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-md-6">
        <div class="card card-shadow" id="widgetLineareaTwo">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon fa-cubes grey-600 font-size-24 vertical-align-bottom mr-5"></i> Barang
              </div>
              <span class="float-right grey-700 font-size-30"><?=$c_barang?></span>
            </div>
            <div class="ct-chart h-50"></div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-md-6">
        <div class="card card-shadow" id="widgetLineareaOne">
          <div class="card-block p-20 pt-10">
            <div class="clearfix">
              <div class="grey-800 float-left py-10">
                <i class="icon fa-cube grey-600 font-size-24 vertical-align-bottom mr-5"></i> Barang sudah & akan Expire
              </div>
              <span class="float-right grey-700 font-size-30"><?=$c_expire?></span>
            </div>
            <div class="ct-chart h-50"></div>
          </div>
        </div>
      </div>

    </div>

    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Barang Sudah & Mendekati Expire</h3>
      </div>

      <div class="panel-body container-fluid">
        <div class="row row-lg">
          <div class="col-12">
            <div class="example-wrap">
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <th width="5%">No.</th>
                        <th width="15%">Tgl. Expire</th>
                        <th width="15%">Kode Barang</th>
                        <th width="35%">Nama Barang</th>
                        <th width="20%">Supplier</th>
                        <th width="10%">Qty</th>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach ($b_expire as $item): ?>
                          <tr>
                            <td><?=$no?></td>
                            <td><?=$item->ExpDate?></td>
                            <td><?=$item->KodeBarang?></td>
                            <td><?=$item->Barang?></td>
                            <td><?=$item->Supplier?></td>
                            <td><?=$item->Qty?></td>
                          </tr>
                        <?php $no++;endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<?php include 'template/footer.php'; ?>
