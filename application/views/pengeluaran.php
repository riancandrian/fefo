<?php include 'template/header.php'; ?>

<div class="page">
  <div class="page-content container-fluid">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">
          Pengeluaran Barang
        </h3>

        <div class="panel-actions">
          <a href="<?=base_url('pengeluaran/add')?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah Pengeluaran</a>
          <!-- <a class="panel-action icon md-fullscreen" data-toggle="panel-fullscreen" aria-hidden="true"></a>
          <a class="panel-action icon md-close" data-toggle="panel-close" aria-hidden="true"></a> -->
        </div>
      </div>

      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
              <div class="example table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th width="10%">No. Ref</th>
                      <th width="10%">Tgl. Keluar</th>
                      <th width="50%">Tujuan</th>
                      <th width="15%" class="text-nowrap">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($pengeluaran as $row): ?>
                      <tr>
                        <td><?=$row->Noref?></td>
                        <td><?=$row->OutDate?></td>
                        <td><?=$row->Keterangan?></td>
                        <td>
                          <a href="<?=base_url('pengeluaran/edit/').$row->Noref?>" class="btn btn-flat btn-success btn-sm"><i class="icon fa-search"></i> Detail</a>
                          <a href="#" class="btn btn-flat btn-info btn-sm"><i class="icon fa-print"></i> Cetak Bukti</a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include 'template/footer.php'; ?>
