<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Login | FEFO</title>

    <link rel="apple-touch-icon" href="<?=base_url();?>assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?=base_url();?>assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/site.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/skins/cyan.css">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/vendor/waves/waves.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/examples/css/pages/login-v3.css">


    <!-- Fonts -->
    <link rel="stylesheet" href="<?=base_url();?>assets/global/fonts/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!-- Scripts -->
    <script src="../../../global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition page-login-v3 layout-full">

    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
      <div class="page-content vertical-align-middle">
        <div class="panel">
          <div class="panel-body">
            <?php if(isset($msg)) :?>
              <div class="row">
                <div class="card card-inverse bg-danger">
                  <div class="card-block">
                    <h4 class="card-title">Maaf !</h4>
                    <p class="card-text">User / Password yang anda masukan salah. Silahkan cek kembali.</p>
                  </div>
                </div>
              </div>
            <?php endif; ?>
            <div class="brand">
              <img class="brand-img" src="<?=base_url();?>assets/images/logo-blue.png" alt="...">
              <h2 class="brand-text font-size-18">FEFO</h2>
            </div>
            <form method="post" action="<?=base_url('login/start')?>" autocomplete="off">
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="text" class="form-control" name="username" />
                <label class="floating-label">Username</label>
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control" name="password" />
                <label class="floating-label">Password</label>
              </div>

              <button type="submit" class="btn btn-primary btn-block btn-lg mt-40" aria-hidden="true">Sign in</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->
    <script src="<?=base_url('assets/');?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/jquery/jquery.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/animsition/animsition.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/waves/waves.js"></script>

    <!-- Plugins -->
    <script src="<?=base_url('assets/');?>global/vendor/switchery/switchery.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/intro-js/intro.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/screenfull/screenfull.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="<?=base_url('assets/');?>global/vendor/jquery-placeholder/jquery.placeholder.js"></script>

    <!-- Scripts -->
    <script src="<?=base_url('assets/');?>global/js/Component.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Base.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Config.js"></script>

    <script src="<?=base_url();?>assets/js/Section/Menubar.js"></script>
    <script src="<?=base_url();?>assets/js/Section/Sidebar.js"></script>
    <script src="<?=base_url();?>assets/js/Section/PageAside.js"></script>
    <script src="<?=base_url();?>assets/js/Plugin/menu.js"></script>

    <!-- Config -->
    <script src="<?=base_url('assets/');?>global/js/config/colors.js"></script>
    <script src="<?=base_url();?>assets/js/config/tour.js"></script>
    <script>Config.set('assets/', '<?=base_url();?>assets');</script>

    <!-- Page -->
    <script src="<?=base_url();?>assets/js/Site.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/asscrollable.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/slidepanel.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/switchery.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/jquery-placeholder.js"></script>
    <script src="<?=base_url('assets/');?>global/js/Plugin/material.js"></script>

    <script>
      (function(document, window, $){
        'use strict';

        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
  </body>
</html>
