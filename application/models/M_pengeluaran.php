<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_pengeluaran extends CI_Model
{
  function index(){
    $data = $this->db->get('tb_pengeluaran')->result();
    return $data;
  }

  function barang(){
    $data = $this->db->get('tb_barang')->result();

    return $data;
  }

  function getListBarang($KodeBarang){
  	$query = $this->db->query("SELECT * from
                                (SELECT b.*
                                   , m.Qty AS QtyMasuk
                                   , m.ExpDate
                                   , ifnull(sum(kd.Qty), 0) AS QtyKeluar
                                   , m.Noref
                                   , (m.Qty - ifnull(sum(kd.Qty), 0)) AS Stok
                              FROM
                                tb_barang b
                              JOIN tb_pemasukan m
                              ON m.KodeBarang = b.KodeBarang
                              LEFT JOIN tb_pengeluaran_det kd
                              ON kd.NorefMasuk = m.Noref
                              GROUP BY
                                m.Noref
                              , m.ExpDate) t
                                where Stok > 0 AND KodeBarang = '".$KodeBarang."' ");

  	$data = $query->result();
  	return $data;
  }

  function getNoref(){
    $date = date('Ym');

    $this->db->like('Noref', $date);
    $cek  = $this->db->get('tb_pengeluaran')->num_rows();

    if($cek > 0){
      $this->db->select('Noref');
      $this->db->from('tb_pengeluaran');
      $this->db->like('Noref', $date);
      $this->db->order_by('Noref', 'DESC');
      $this->db->limit(1);

      $data = $this->db->get()->row_array();
      $last = substr($data['Noref'], -3);
      $count= $last + 1;
      $new  = str_pad($count,3,"0",STR_PAD_LEFT);

      $noref = 'OUT'.$date.$new;

    }else{
      $noref = 'OUT'.$date.'001';
    }

    return $noref;
  }

  function insertHead($data){
  	return $this->db->insert('tb_pengeluaran', $data);
  }

  function insertDet($data){
  	return $this->db->insert('tb_pengeluaran_det', $data);
  }

  function detail($noref){
    $query = "SELECT a.*, b.Barang, b.Satuan, c.ExpDate
                FROM tb_pengeluaran_det a
                JOIN tb_barang b ON a.KodeBarang = b.KodeBarang
                JOIN tb_pemasukan c ON c.Noref = a.NorefMasuk
                WHERE a.Noref = '".$noref."'
                ";
    $data  = $this->db->query($query)->result();

    return $data;
  }
}
