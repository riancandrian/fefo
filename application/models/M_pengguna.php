<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_pengguna extends CI_Model
{

  function getIdPengguna(){
    $date = date('Ym');

    $this->db->like('IdPengguna', $date);
    $cek  = $this->db->get('tb_pengguna')->num_rows();

    if($cek > 0){
      $this->db->select('IdPengguna');
      $this->db->from('tb_pengguna');
      $this->db->like('IdPengguna', $date);
      $this->db->order_by('IdPengguna', 'DESC');
      $this->db->limit(1);

      $data = $this->db->get()->row_array();
      $last = substr($data['IdPengguna'], -3);
      $count= $last + 1;
      $new  = str_pad($count,3,"0",STR_PAD_LEFT);

      $idPengguna = 'USER'.$date.$new;

    }else{
      $idPengguna = 'USER'.$date.'001';
    }

    return $idPengguna;
  }

  function get($id = NULL){
    if($id){
      $data = $this->db->get_where('tb_pengguna', array('IdPengguna' => $id))->result();
    }else{
      $data = $this->db->get('tb_pengguna')->result();
    }

    return $data;
  }

  function insert($data){
    return $this->db->insert('tb_pengguna', $data);
  }

  function update($data, $where){
    return $this->db->update('tb_pengguna', $data, $where);
  }

}
