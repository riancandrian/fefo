<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_pemasukan extends CI_Model
{

  function getNoref(){
    $date = date('Ym');

    $this->db->like('Noref', $date);
    $cek  = $this->db->get('tb_pemasukan')->num_rows();

    if($cek > 0){
      $this->db->select('Noref');
      $this->db->from('tb_pemasukan');
      $this->db->like('Noref', $date);
      $this->db->order_by('Noref', 'DESC');
      $this->db->limit(1);

      $data = $this->db->get()->row_array();
      $last = substr($data['Noref'], -3);
      $count= $last + 1;
      $new  = str_pad($count,3,"0",STR_PAD_LEFT);

      $noref = 'IN'.$date.$new;

    }else{
      $noref = 'IN'.$date.'001';
    }

    return $noref;
  }

  function get($id = NULL){
    $this->db->join('tb_supplier', 'tb_pemasukan.IdSupplier = tb_supplier.IdSupplier');
    $this->db->join('tb_barang', 'tb_pemasukan.KodeBarang = tb_barang.KodeBarang');
    if($id){
      $data = $this->db->get_where('tb_pemasukan', array('Noref' => $id))->result();
    }else{
      $data = $this->db->get('tb_pemasukan')->result();
    }

    return $data;
  }

  function insert($data){
    return $this->db->insert('tb_pemasukan', $data);
  }

  function update($data, $where){
    return $this->db->update('tb_pemasukan', $data, $where);
  }

  function getIndex(){
    $this->db->join('tb_barang', 'tb_barang.KodeBarang = tb_pemasukan.KodeBarang');
    $this->db->join('tb_supplier', 'tb_supplier.IdSupplier = tb_pemasukan.IdSupplier');
    $data = $this->db->get('tb_pemasukan')->result();

    return $data;
  }
}
