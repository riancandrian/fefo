<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_laporan extends CI_Model
{
  function persediaan(){
    $query = "SELECT b.*
              , k.Kategori
              , IFNULL(SUM(m.Qty), 0) AS QtyMasuk
              , IFNULL(t.QtyKeluar, 0) AS QtyKeluar
              , (IFNULL(SUM(m.Qty), 0) - IFNULL(t.QtyKeluar, 0)) AS Sisa
              , m.ExpDate
              FROM tb_barang b
              JOIN tb_kategori k ON k.IdKat = b.IdKat
              LEFT JOIN tb_pemasukan m ON b.KodeBarang = m.KodeBarang
              LEFT JOIN (SELECT d.KodeBarang
              			, IFNULL(SUM(d.Qty), 0) AS QtyKeluar
              			FROM tb_pengeluaran_det d
              			GROUP BY d.KodeBarang) t ON t.KodeBarang = b.KodeBarang
              GROUP BY b.KodeBarang
              ";

    $data = $this->db->query($query)->result();

    return $data;
  }

  function persediaan_det($kode){
    $query = "SELECT b.*
                , m.ExpDate
                , m.Qty AS Masuk
                , IFNULL(k.Qty, 0) AS Keluar
                , (m.Qty - IFNULL(k.Qty, 0)) AS Sisa
                , l.Kategori
                from tb_barang b
                JOIN tb_pemasukan m ON m.KodeBarang = b.KodeBarang
                LEFT JOIN tb_pengeluaran_det k ON k.KodeBarang = m.KodeBarang AND m.Noref = k.NorefMasuk
                JOIN tb_kategori l ON l.IdKat = b.IdKat
                where b.KodeBarang = '".$kode."'
                ";

    $data = $this->db->query($query)->result();

    return $data;
  }

  function pemasukan($where = NULL){
    if($where){
      $this->db->where('EntryDate BETWEEN "'.$where['start'].'" AND "'.$where['end'].'" ');
    }
    $this->db->join('tb_supplier', 'tb_supplier.IdSupplier = tb_pemasukan.IdSupplier');
    $this->db->join('tb_barang', 'tb_barang.KodeBarang = tb_pemasukan.KodeBarang');
    $data = $this->db->get('tb_pemasukan')->result();

    return $data;
  }

  function pengeluaran($where = NULL){
    if($where){
      $this->db->where('OutDate BETWEEN "'.$where['start'].'" AND "'.$where['end'].'" ');
    }
    $this->db->join('tb_pengeluaran_det', 'tb_pengeluaran.Noref = tb_pengeluaran_det.Noref');
    $this->db->join('tb_barang', 'tb_barang.KodeBarang = tb_pengeluaran_det.KodeBarang');
    $this->db->join('tb_pemasukan', 'tb_pemasukan.Noref = tb_pengeluaran_det.NorefMasuk');
    $data = $this->db->get('tb_pengeluaran')->result();

    return $data;
  }
}
