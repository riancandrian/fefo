<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_kategori extends CI_Model
{

  function getIdkat(){
    $date = date('Ym');

    $this->db->like('IdKat', $date);
    $cek  = $this->db->get('tb_kategori')->num_rows();

    if($cek > 0){
      $this->db->select('IdKat');
      $this->db->from('tb_kategori');
      $this->db->like('IdKat', $date);
      $this->db->order_by('IdKat', 'DESC');
      $this->db->limit(1);

      $data = $this->db->get()->row_array();
      $last = substr($data['IdKat'], -2);
      $count= $last + 1;
      $new  = str_pad($count,2,"0",STR_PAD_LEFT);

      $idKat = 'CAT'.$date.$new;

    }else{
      $idKat = 'CAT'.$date.'01';
    }

    return $idKat;
  }

  function get($id = NULL){
    if($id){
      $data = $this->db->get_where('tb_kategori', array('IdKat' => $id))->result();
    }else{
      $data = $this->db->get('tb_kategori')->result();
    }

    return $data;
  }

  function insert($data){
    return $this->db->insert('tb_kategori', $data);
  }

  function update($data, $where){
    return $this->db->update('tb_kategori', $data, $where);
  }

}
