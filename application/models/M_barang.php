<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_barang extends CI_Model
{

  function getKodeBarang(){
    $date = date('Ym');

    $this->db->like('KodeBarang', $date);
    $cek  = $this->db->get('tb_barang')->num_rows();

    if($cek > 0){
      $this->db->select('KodeBarang');
      $this->db->from('tb_barang');
      $this->db->like('KodeBarang', $date);
      $this->db->order_by('KodeBarang', 'DESC');
      $this->db->limit(1);

      $data = $this->db->get()->row_array();
      $last = substr($data['KodeBarang'], -3);
      $count= $last + 1;
      $new  = str_pad($count,3,"0",STR_PAD_LEFT);

      $kdBarang = 'BRG'.$date.$new;

    }else{
      $kdBarang = 'BRG'.$date.'001';
    }

    return $kdBarang;
  }

  function get($id = NULL){

    $this->db->join('tb_kategori', 'tb_kategori.IdKat = tb_barang.IdKat');

    if($id){
      $data = $this->db->get_where('tb_barang', array('KodeBarang' => $id))->result();
    }else{
      $data = $this->db->get('tb_barang')->result();
    }

    return $data;
  }

  function insert($data){
    return $this->db->insert('tb_barang', $data);
  }

  function update($data, $where){
    return $this->db->update('tb_barang', $data, $where);
  }

  function getBarangExpire(){
    $query = "SELECT tb_barang.KodeBarang
                     , tb_barang.Barang
                     , tb_barang.Satuan
                     , tb_pemasukan.Qty AS Masuk
                     , ifnull(tb_pengeluaran_det.Qty, 0) AS keluar
                     , tb_pemasukan.ExpDate
                     , tb_supplier.Supplier
                     , (tb_pemasukan.Qty - ifnull(tb_pengeluaran_det.Qty, 0)) AS Qty
                FROM
                  tb_barang
                JOIN tb_pemasukan
                ON tb_pemasukan.KodeBarang = tb_barang.KodeBarang
                JOIN tb_supplier
                ON tb_supplier.IdSupplier = tb_pemasukan.IdSupplier
                LEFT JOIN tb_pengeluaran_det
                ON tb_pengeluaran_det.NorefMasuk = tb_pemasukan.Noref
                WHERE
                  tb_pemasukan.ExpDate BETWEEN date_add(curdate(), INTERVAL -1 MONTH) AND date_add(curdate(), INTERVAL 1 MONTH)
                  AND (tb_pemasukan.Qty - ifnull(tb_pengeluaran_det.Qty, 0)) > 0
                ORDER BY
                  Barang
                , ExpDate;";
    $data = $this->db->query($query);

    return $data;
  }
}
