<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class M_supplier extends CI_Model
{

  function getIdSup(){
    $date = date('Ym');

    $this->db->like('IdSupplier', $date);
    $cek  = $this->db->get('tb_supplier')->num_rows();

    if($cek > 0){
      $this->db->select('IdSupplier');
      $this->db->from('tb_supplier');
      $this->db->like('IdSupplier', $date);
      $this->db->order_by('IdSupplier', 'DESC');
      $this->db->limit(1);

      $data = $this->db->get()->row_array();
      $last = substr($data['IdSupplier'], -3);
      $count= $last + 1;
      $new  = str_pad($count,3,"0",STR_PAD_LEFT);

      $idSup = 'SUP'.$date.$new;

    }else{
      $idSup = 'SUP'.$date.'001';
    }

    return $idSup;
  }

  function get($id = NULL){
    if($id){
      $data = $this->db->get_where('tb_supplier', array('IdSupplier' => $id))->result();
    }else{
      $data = $this->db->get('tb_supplier')->result();
    }

    return $data;
  }

  function insert($data){
    return $this->db->insert('tb_supplier', $data);
  }

  function update($data, $where){
    return $this->db->update('tb_supplier', $data, $where);
  }

}
