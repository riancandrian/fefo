<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	  	$this->load->model('m_barang', 'barang');
	  	if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$data['kategori'] = $this->db->get('tb_kategori')->result();
		$this->load->view('barang', $data);
	}

	public function getBarangExpire(){
		$data = $this->barang->getBarangExpire();
	}

	public function get()
	{
		$data = $this->barang->get();

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function getById()
	{
		$id   = $this->input->post('id');
		$data = $this->barang->get($id);

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function saveorup_barang()
	{
		$data 			= $this->_valuesHead();
		$KodeBarang = $this->input->post('KodeBarang');

		if($KodeBarang != ''){
			$where['KodeBarang'] = $this->input->post('KodeBarang');
			$this->barang->update($data, $where);
		}else{
			$data['KodeBarang'] = $this->barang->getKodeBarang();
			$this->barang->insert($data);
		}

		echo json_encode(array('success' => true));
	}

	public function _valuesHead()
	{
		$data = array(
			'Barang' => $this->input->post('Barang'),
			'Satuan' => $this->input->post('Satuan'),
			'IdKat' => $this->input->post('IdKat'),
		);

		return $data;
	}

	public function hapus()
	{
		$kdBarang = $this->input->post('id');

		//cek apakah ada pemakaian Barang
		$cek = $this->db->get_where('tb_pemasukan', array('KodeBarang' => $kdBarang))->num_rows();
		if($cek > 0){

			echo json_encode(array('success' => false));
		}else{
			$this->db->where('KodeBarang', $kdBarang);
			$this->db->delete('tb_barang');

			echo json_encode(array('success' => true));
		}
	}
}
