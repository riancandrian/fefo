<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukan extends CI_Controller
{
	public function __construct(){
		parent::__construct();
	  	$this->load->model('m_pemasukan', 'masuk');
			if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$data['pemasukan'] 	= $this->masuk->getIndex();
		$data['barang'] 		= $this->db->get('tb_barang')->result();
		$data['supplier'] 	= $this->db->get('tb_supplier')->result();
		$this->load->view('pemasukan', $data);
	}

	public function get()
	{
		$data = $this->masuk->get();

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function getById()
	{
		$id   = $this->input->post('id');
		$data = $this->masuk->get($id);

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function saveorup_pemasukan()
	{
		$data 	= $this->_valuesHead();
		$Noref 	= $this->input->post('Noref');

		if($Noref != ''){
			$where['Noref'] = $this->input->post('Noref');
			$this->masuk->update($data, $where);
		}else{
			$data['Noref'] = $this->masuk->getNoref();
			$this->masuk->insert($data);
		}

		echo json_encode(array('success' => true));
	}

	public function _valuesHead()
	{
		$entry = date('Y-m-d', strtotime($this->input->post('EntryDate')));
		$exp   = date('Y-m-d', strtotime($this->input->post('ExpDate')));

		$data = array(
			'EntryDate' 	=> $entry,
			'KodeBarang' 	=> $this->input->post('KodeBarang'),
			'IdSupplier' 	=> $this->input->post('IdSupplier'),
			'Qty' 				=> $this->input->post('Qty'),
			'ExpDate' 		=> $exp,
		);

		return $data;
	}

	public function hapus()
	{
		$noref = $this->input->post('id');
		$this->db->where('Noref', $noref);
		$this->db->delete('tb_pemasukan');

		echo json_encode(array('success' => true));
	}
}
