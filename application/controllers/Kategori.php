<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller
{
	public function __construct(){
		parent::__construct();
	  	$this->load->model('m_kategori', 'm_kat');
			if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$this->load->view('kategori');
	}

	public function get()
	{
		$data = $this->m_kat->get();

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function getById()
	{
		$id   = $this->input->post('id');
		$data = $this->m_kat->get($id);

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function saveorup_kategori()
	{
		$data 	= array('Kategori' => $this->input->post('Kategori'));
		$idKat 	= $this->input->post('IdKat');

		if($idKat != 'Auto Generate'){
			$where['IdKat'] = $this->input->post('IdKat');
			$this->m_kat->update($data, $where);
		}else{
			$data['IdKat'] = $this->m_kat->getIdkat();
			$this->m_kat->insert($data);
		}

		echo json_encode(array('success' => true));
	}

	public function hapus()
	{
		$idKat = $this->input->post('id');
		$this->db->where('IdKat', $idKat);
		$this->db->delete('tb_kategori');

		echo json_encode(array('success' => true));
	}
}
