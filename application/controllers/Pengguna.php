<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
	public function __construct(){
		parent::__construct();
	  	$this->load->model('m_pengguna', 'pengguna');
			if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$this->load->view('pengguna');
	}

	public function get()
	{
		$data = $this->pengguna->get();
		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function getById()
	{
		$id   = $this->input->post('id');
		$data = $this->pengguna->get($id);

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function saveorup_pengguna()
	{
		$data 				= $this->_valuesHead();
		$password 		= $this->input->post('Password');
		$idPengguna 	= $this->input->post('IdPengguna');

		if($idPengguna){
			$where['IdPengguna'] = $this->input->post('IdPengguna');

			if($password){
				$data['Password']  = md5($this->input->post('Password'));
			}
			$this->pengguna->update($data, $where);

		}else{
			$data['Password']  	 = md5($this->input->post('Password'));
			$data['IdPengguna']  = $this->pengguna->getIdPengguna();
			$this->pengguna->insert($data);
		}

		echo json_encode(array('success' => true));
	}

	public function _valuesHead()
	{
		$data = array(
			'NamaPengguna' => $this->input->post('NamaPengguna'),
			'Username' => $this->input->post('Username'),
			'Role' => $this->input->post('Role'),
		);

		return $data;
	}

	public function hapus()
	{
		$idPengguna = $this->input->post('id');
		$this->db->where('IdPengguna', $idPengguna);
		$this->db->delete('tb_pengguna');

		echo json_encode(array('success' => true));
	}
}
