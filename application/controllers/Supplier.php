<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller
{
	public function __construct(){
		parent::__construct();
	  	$this->load->model('m_supplier', 'supplier');
			if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$this->load->view('supplier');
	}

	public function get()
	{
		$data = $this->supplier->get();

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function getById()
	{
		$id   = $this->input->post('id');
		$data = $this->supplier->get($id);

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function saveorup_supplier()
	{
		$data 			= $this->_valuesHead();
		$idSupplier = $this->input->post('IdSupplier');

		if($idSupplier != ''){
			$where['IdSupplier'] = $this->input->post('IdSupplier');
			$this->supplier->update($data, $where);
		}else{
			$data['IdSupplier'] = $this->supplier->getIdSup();
			$this->supplier->insert($data);
		}

		echo json_encode(array('success' => true));
	}

	public function _valuesHead()
	{
		$data = array(
			'Supplier' => $this->input->post('Supplier'),
			'Alamat' => $this->input->post('Alamat'),
			'Phone' => $this->input->post('Phone'),
		);

		return $data;
	}

	public function hapus()
	{
		$idSup = $this->input->post('id');
		$this->db->where('IdSupplier', $idSup);
		$this->db->delete('tb_supplier');

		echo json_encode(array('success' => true));
	}
}
