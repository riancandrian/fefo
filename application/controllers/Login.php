<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->model('m_login', 'login');
  }

	public function index()
	{
		$this->load->view('login');
	}

	function logout()
  {
    $this->session->sess_destroy();
    redirect('login');
  }

	function start()
  {
  	$user = $this->input->post('username');
  	$pass = $this->input->post('password');

  	$res  = $this->login->get_auth($user, $pass);
  	$count= $res->num_rows();

  	if($count > 0){
  		$data = $res->row_array();

  		$session_array = array(
  			'is_login' => true,
  			'user_id'  => $data['IdPengguna'],
  			'username' => $data['Username'],
  			'nm_user'  => $data['NamaPengguna']
  		);

  		$this->session->set_userdata($session_array);
  		redirect('dashboard/index');

  	}else{
  		$this->load->view('login', array('msg' => 'login failed'));
  	}
  }

}
