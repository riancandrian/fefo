<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends CI_Controller
{
	public function __construct(){
		parent::__construct();
	  	$this->load->model('m_pengeluaran', 'pengeluaran');
			if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$data['pengeluaran'] = $this->pengeluaran->index();

		$this->load->view('pengeluaran', $data);
	}

	public function add()
	{
		$data['barang'] = $this->pengeluaran->barang();
		$this->load->view('pengeluaran_add', $data);
	}

	public function getListBarang()
	{
		$kodeBarang = $this->input->post('kode');
		$data = $this->pengeluaran->getListBarang($kodeBarang);

		echo json_encode(array('success' => true, 'data' => $data));
	}

	public function saveorup_pengeluaran()
	{
		$data 	= $this->_valuesHead();
		$noref 	= $this->input->post('Noref');

		if($noref){

		}else{
			$data['Noref'] = $this->pengeluaran->getNoref();
			$this->pengeluaran->insertHead($data);

			$noref = $data['Noref'];
			$this->_insertDet($noref);
		}

		echo json_encode(array('success' => true));
	}

	public function _valuesHead()
	{
		$data = array('OutDate' 	=> date('Y-m-d'),
					  'Keterangan'	=> $this->input->post('Keterangan'),
					);

		return $data;
	}

	public function _insertDet($noref)
	{
		$kodeBarang = $this->input->post('Kode');

		foreach ($kodeBarang as $key => $kode) {
			$dataDet = array(
								'KodeBarang' 	=> $kode,
							 	'Qty' 				=> $this->input->post('Qty')[$key],
							 	'NorefMasuk' 	=> $this->input->post('NorefMasuk')[$key],
							 	'Noref' 			=> $noref
							);

			$this->pengeluaran->insertDet($dataDet);
		}

		return true;
	}

	public function edit(){
		$noref 	= $this->uri->segment(3);
		$data['detail'] = $this->pengeluaran->detail($noref);

		$this->load->view('pengeluaran_detail', $data);
	}
}
