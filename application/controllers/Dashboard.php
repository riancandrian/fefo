<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
			$this->load->model('m_barang', 'barang');
	  	if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	public function index()
	{
		$data['c_pengguna']	= $this->db->get('tb_pengguna')->num_rows();
		$data['c_barang'] 	= $this->db->get('tb_barang')->num_rows();
		$data['c_supplier'] = $this->db->get('tb_supplier')->num_rows();
		$data['b_expire']		= $this->barang->getBarangExpire()->result();
		$data['c_expire']		= $this->barang->getBarangExpire()->num_rows();
		$this->load->view('index', $data);
	}
}
