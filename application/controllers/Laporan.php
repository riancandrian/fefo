<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller
{
	public function __construct(){
		parent::__construct();
			$this->load->model('m_laporan', 'laporan');
			if(!$this->session->userdata('is_login')){
	  		redirect('login');
	  	}
	}

	// PERSEDIAAN
	public function persediaan()
	{
		$data['barang'] = $this->laporan->persediaan();
		$this->load->view('lap_persediaan', $data);
	}

	public function persediaan_det()
	{
		$kodebarang = $this->uri->segment(3);
		$data['detail'] = $this->laporan->persediaan_det($kodebarang);
		$this->load->view('lap_persediaan_det', $data);
	}

	public function persediaan_pdf(){
		$data['datanya'] = $this->laporan->persediaan();
		$this->load->view('cetakan/persediaan_pdf', $data);
	}

	// PEMASUKAN
  public function pemasukan()
	{
		$data['pemasukan'] = $this->laporan->pemasukan();
		$this->load->view('lap_pemasukan', $data);
	}

	public function filter_pemasukan(){
		$start = date('Ymd',strtotime($this->input->post('start')));
		$end 	 = date('Ymd',strtotime($this->input->post('end')));
		$where = array('start' => $start, 'end' => $end);

		$data['start']		 = $this->input->post('start');
		$data['end']		 	 = $this->input->post('end');
		$data['pemasukan'] = $this->laporan->pemasukan($where);
		$this->load->view('lap_pemasukan', $data);
	}

	public function pemasukan_pdf(){
		$start = $this->input->get('start');
		$end 	 = $this->input->get('end');
		$where = '';

		if($start != ''){$start = date('Ymd',strtotime($start)); }
		if($end != ''){$end = date('Ymd',strtotime($end)); }

		if($start != '' && $end != ''){
			$where = array('start' => $start, 'end' => $end);
		}

		$data['start'] = $start;
		$data['end']	 = $end;
		$data['datanya'] = $this->laporan->pemasukan($where);
		$this->load->view('cetakan/pemasukan_pdf', $data);

	}

	// PENGELUARAN
  public function pengeluaran()
	{
		$data['pengeluaran'] = $this->laporan->pengeluaran();
		$this->load->view('lap_pengeluaran', $data);
	}

	public function filter_pengeluaran(){
		$start = date('Ymd',strtotime($this->input->post('start')));
		$end 	 = date('Ymd',strtotime($this->input->post('end')));
		$where = array('start' => $start, 'end' => $end);

		$data['start']		 = $this->input->post('start');
		$data['end']		 	 = $this->input->post('end');
		$data['pengeluaran'] = $this->laporan->pengeluaran($where);
		$this->load->view('lap_pengeluaran', $data);
	}

	public function pengeluaran_pdf(){
		$start = $this->input->get('start');
		$end 	 = $this->input->get('end');
		$where = '';

		if($start != ''){$start = date('Ymd',strtotime($start)); }
		if($end != ''){$end = date('Ymd',strtotime($end)); }

		if($start != '' && $end != ''){
			$where = array('start' => $start, 'end' => $end);
		}

		$data['start'] = $start;
		$data['end']	 = $end;
		$data['datanya'] = $this->laporan->pengeluaran($where);
		$this->load->view('cetakan/pengeluaran_pdf', $data);

	}
}
